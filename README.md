# nccdoc

nccdoc - documentation for networking on linux.

## About

This project contains documentation for configuring: routing, bridging,
vlans, dhcp, DNS, etc. within linux.

## Building

To build from source [scdoc](https://git.sr.ht/~sircmpwn/scdoc) is
required.

The target `html` generates static html in the `build/` directory. The
target `man` generates `groff` files. `install` adds these to
`/usr/local/share/man` by default.

```
make man html
sudo make install
```

## Legal

[nccdoc](https://nccdoc.cyb3r.lol) by
[Luke Spademan](https://lspade.xyz/index.asc.txt) is licensed under
[CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)
